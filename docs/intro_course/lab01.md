We are going to work with a QEMU wrapper, called _virtme_. _Virtme_ is a set of simple tools to run a virtualized Linux kernel. It can boot a kernel using the host Linux distribution or a simple rootfs instead of a whole disk image. Virtme is tiny, easy to use, and makes testing kernel changes quite simple. You can read more about QEMU [here](https://wiki.qemu.org/Documentation), and more about virtme [here](https://www.collabora.com/news-and-blog/blog/2018/09/18/virtme-the-kernel-developers-best-friend).

In order to work through the exercises, we need to setup a couple things first.
 
!!! warning
    Run these steps before the course. We won't have time to fix individual setups during the classes.

#### Getting dependencies

For Ubuntu 16.04, Ubuntu 18.x, Debian Jessie or Debian Stretch,
the following will work:

```bash
apt-get -y -q install          \
  bc                           \
  flex                         \
  bison                        \
  build-essential              \
  git                          \
  libncurses-dev               \
  libssl-dev                   \
  libelf-dev                   \
  u-boot-tools                 \
  wget                         \
  xz-utils                     \
  qemu-kvm                     \
  iproute2                     \
  python3                      \
  python3-pip
```

The package list may be different on other Ubuntu versions, or other distributions.

#### Installing virtme

```bash
pip3 install --user git+https://github.com/ezequielgarcia/virtme.git
```

#### Building a kernel

```bash
cd ~
git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
cd linux
virtme-configkernel --defconfig
make oldconfig
make -j8
```

#### Running your kernel

```bash
cd ~/linux
virtme-run --kimg arch/x86/boot/bzImage
[...]
```

If everything went OK, you'll be booting a kernel inside a QEMU instance. In order to quit the QEMU instance, you can use **Ctrl-A** **X**.
